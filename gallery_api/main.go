package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"gallery_api/config"
	"gallery_api/handlers"
	"gallery_api/models"
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {

	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if conf.Mode == "dev" {
		db.LogMode(true) // dev only!
	}

	if err := db.AutoMigrate(
		&models.GalleryTable{},
		&models.ImageTable{},
		&models.UserTable{},
		&models.LikeTable{},
	).Error; err != nil {
		log.Fatal(err)
	}

	mac := hmac.New(sha256.New, []byte(conf.HMACKey))

	us := models.NewUserGorm(db, mac)

	uh := handlers.NewUserHandler(us, mac)

	gs := models.NewGalleryGorm(db)

	gh := handlers.NewGalleryHandler(gs)

	is := models.NewInteractiveGorm(db)

	ih := handlers.NewInteractiveHandler(is)

	if conf.Mode != "dev" {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.Default()
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"*"}
	config.AllowHeaders = []string{"*"}

	r.Use(cors.New(config))

	r.Use(static.Serve("/images", static.LocalFile("./upload", true)))

	authorized := r.Group("/")
	authorized.Use(uh.Authorization)
	{
		authorized.GET("/user", uh.GetUserData)

		authorized.GET("/user/:id", uh.GetUserDataById)

		authorized.GET("/search", uh.GetUserBySearch)

		authorized.GET("/allgallery/:id", gh.GetAllGallery)

		authorized.GET("/allimage", gh.GetAllImage)

		authorized.GET("/allimagesort", gh.GetAllImageSort)

		authorized.GET("/gallery/:id", gh.GetGallery)

		authorized.GET("/gallerybyuser/:user_id", gh.GetGalleryBYUserID)

		authorized.GET("/imagelist/:gallery_id", gh.GetImageList)

		authorized.GET("/like/:img_id", ih.GetLikeById)

		authorized.POST("/gallery", gh.CreateGallery)

		authorized.POST("/image/:id", gh.CreateImage)

		authorized.POST("/like/:img_id/:user_id", ih.CreateLike)

		authorized.PATCH("/namegallery/:id", gh.UpdateGalleryName)

		authorized.PATCH("/covergallery/:id", gh.UpdateGalleryCover)

		authorized.PATCH("/statusgallery/:id", gh.UpdateGalleryStatus)

		authorized.PATCH("/user/:id", uh.UpdateProfileImg)

		authorized.PATCH("/logout/:id", uh.Logout)

		authorized.DELETE("/gallery/:id", gh.DeleteGallery)

		authorized.DELETE("/allimage/:id", gh.DeleteAllImage)

		authorized.DELETE("/image/:id", gh.DeleteImage)

	}

	r.POST("/login", uh.Login)

	r.POST("/signup", uh.Signup)

	r.Run()
}
