package rand

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
)

func GetToken() (string, error) {
	key := "bowwie"
	hm := hmac.New(sha256.New, []byte(key))
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	hm.Write([]byte(b))

	hash := hm.Sum(nil)

	return base64.URLEncoding.EncodeToString(hash), nil
}
