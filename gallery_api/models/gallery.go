package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

type GalleryTable struct {
	gorm.Model
	Name   string
	Cover  string
	Status bool
	UserID uint
}

func (GalleryTable) TableName() string {
	return "gallerys"
}

type ImageTable struct {
	gorm.Model
	Name      string
	Image     string
	GalleryID uint
}

type Gallery struct {
	ID     uint
	Name   string
	Cover  string
	Status bool
	UserID uint
}

func (ImageTable) TableName() string {
	return "images"
}

type ListImage struct {
	ID          uint
	Name        string
	Image       string
	GalleryID   uint
	UserProfile string
	UserEmail   string
	UserID      uint
}

type GalleryService interface {
	GetAllGallery(user_id uint) ([]GalleryTable, error)
	GetAllImage() ([]ListImage, error)
	GetAllImageSort() ([]ListImage, error)
	GetGallery(gallery_id uint) ([]GalleryTable, error)
	GetGalleryBYUserID(user_id uint) ([]GalleryTable, error)
	GetImageList(gallery_id uint) ([]ImageTable, error)

	UpdateGalleryName(id uint, name string) error
	UpdateGalleryStatus(id uint, status bool) error
	UpdateGalleryCover(id uint, cover string) error

	DeleteGallery(id uint) error
	DeleteAllImage(id uint) error
	DeleteImage(id uint) error

	CreateGallery(gallery *GalleryTable) error
	CreateImage(image *ImageTable) error
}

// type UpdateStatus struct {
// 	ID     uint
// 	Status bool
// }

type GalleryGorm struct {
	db *gorm.DB
}

func NewGalleryGorm(db *gorm.DB) GalleryService {
	return &GalleryGorm{db}
}

func (gs *GalleryGorm) CreateGallery(gallery *GalleryTable) error {
	return gs.db.Create(gallery).Error
}

func (gs *GalleryGorm) GetAllGallery(user_id uint) ([]GalleryTable, error) {
	galleryTables := []GalleryTable{}
	if err := gs.db.Where("user_id = ? ", user_id).Find(&galleryTables).Error; err != nil {
		return nil, err
	}
	return galleryTables, nil
}

func (gs *GalleryGorm) GetAllImage() ([]ListImage, error) {
	imageTable := []ListImage{}
	rows, err := gs.db.Table("images").
		Select("images.id,images.name,images.image,images.gallery_id,users.profile,users.email,users.id").
		Joins("join gallerys on images.gallery_id = gallerys.id").
		Joins("join users on users.id = gallerys.user_id").
		Where("gallerys.Status = ? AND images.deleted_at is NULL", true).
		Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		report := ListImage{}
		err := rows.Scan(&report.ID, &report.Name, &report.Image, &report.GalleryID, &report.UserProfile, &report.UserEmail, &report.UserID)
		if err != nil {
			fmt.Println(err)
		}
		imageTable = append(imageTable, report)
	}
	if err != nil {
		return nil, err
	}
	return imageTable, nil
}

func (gs *GalleryGorm) GetAllImageSort() ([]ListImage, error) {
	imageTable := []ListImage{}
	rows, err := gs.db.Table("images").
		Select("images.id,images.name,images.image,images.gallery_id,users.profile,users.email,users.id").
		Joins("join gallerys on images.gallery_id = gallerys.id").
		Joins("join users on users.id = gallerys.user_id").
		Where("gallerys.Status = ? AND images.deleted_at is NULL", true).
		Order("images.id desc").
		Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		report := ListImage{}
		err := rows.Scan(&report.ID, &report.Name, &report.Image, &report.GalleryID, &report.UserProfile, &report.UserEmail, &report.UserID)
		if err != nil {
			fmt.Println(err)
		}
		imageTable = append(imageTable, report)
	}
	if err != nil {
		return nil, err
	}
	return imageTable, nil
}

func (gs *GalleryGorm) GetGallery(gallery_id uint) ([]GalleryTable, error) {
	galleryTables := []GalleryTable{}
	if err := gs.db.Where("id = ? ", gallery_id).Find(&galleryTables).Error; err != nil {
		return nil, err
	}
	return galleryTables, nil
}

func (gs *GalleryGorm) GetGalleryBYUserID(user_id uint) ([]GalleryTable, error) {
	galleryTables := []GalleryTable{}
	if err := gs.db.Where("user_id = ? ", user_id).Find(&galleryTables).Error; err != nil {
		return nil, err
	}
	return galleryTables, nil
}

func (gs *GalleryGorm) GetImageList(id uint) ([]ImageTable, error) {
	imageTables := []ImageTable{}
	if err := gs.db.Where("gallery_id=?", id).Find(&imageTables).Error; err != nil {
		return nil, err
	}
	return imageTables, nil
}

func (gs *GalleryGorm) UpdateGalleryName(id uint, name string) error {
	return gs.db.Model(&GalleryTable{}).Where("id = ?", id).Update("name", name).Error
}

func (gs *GalleryGorm) UpdateGalleryStatus(id uint, status bool) error {
	return gs.db.Model(&GalleryTable{}).Where("id = ?", id).Update("status", status).Error
}

func (gs *GalleryGorm) UpdateGalleryCover(id uint, cover string) error {
	return gs.db.Model(&GalleryTable{}).Where("id = ?", id).Update("cover", cover).Error
}

func (gs *GalleryGorm) CreateImage(image *ImageTable) error {
	return gs.db.Create(image).Error
}

func (gs *GalleryGorm) DeleteGallery(id uint) error {
	galleryTable := GalleryTable{}
	return gs.db.Where("id = ?", id).Delete(&galleryTable).Error

}

func (gs *GalleryGorm) DeleteAllImage(id uint) error {
	imageTable := ImageTable{}
	return gs.db.Where("gallery_id = ?", id).Delete(&imageTable).Error
}

func (gs *GalleryGorm) DeleteImage(id uint) error {
	imageTable := ImageTable{}
	return gs.db.Where("id = ?", id).Delete(&imageTable).Error
}
