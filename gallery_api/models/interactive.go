package models

import (
	"github.com/jinzhu/gorm"
)

type LikeTable struct {
	gorm.Model
	UserID  uint
	ImageID uint
}

func (LikeTable) TableName() string {
	return "likes"
}

// {
// 	name: 'test',
// 	images: [
// 		{
// 			id:1,
// 			likes: [{
// 				user_id: 1
// 				user_id: 3
// 			}]
// 		}
// 	]
// }

type InteractiveService interface {
	GetLikeById(image_id uint) ([]LikeTable, error)
	CreateLike(like *LikeTable) error
}

type InteractiveGorm struct {
	db *gorm.DB
}

func NewInteractiveGorm(db *gorm.DB) InteractiveService {
	return &InteractiveGorm{db}
}

func (is *InteractiveGorm) CreateLike(like *LikeTable) error {
	return is.db.Create(like).Error
}

func (is *InteractiveGorm) GetLikeById(image_id uint) ([]LikeTable, error) {
	likeTable := []LikeTable{}
	if err := is.db.Where("image_id = ?", image_id).Find(&likeTable).Error; err != nil {
		return nil, err
	}
	return likeTable, nil
}
