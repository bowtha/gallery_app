package models

import (
	"encoding/base64"
	"gallery_api/rand"
	"hash"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type Login struct {
	Email    string
	Password string
}

type UserTable struct {
	gorm.Model
	Email    string `gorm:"unique_index;not null"`
	Password string `gorm:"not null"`
	Profile  string
	Token    string
}

func (UserTable) TableName() string {
	return "users"
}

func (Login) TableName() string {
	return "logins"
}

type UserService interface {
	Signup(user *UserTable) error
	Login(user *Login) (string, error)
	GetUsers() ([]UserTable, error)
	GetUserByToken(token string) (*UserTable, error)
	UpdateProfileImg(user_id uint, profile string) error
	GetUserBySearch(search string) ([]UserTable, error)
	GetUserDataById(user_id uint) (*UserTable, error)
	Logout(user_id uint) error
}

type UserGorm struct {
	db   *gorm.DB
	hmac hash.Hash
}

func NewUserGorm(db *gorm.DB, hmac hash.Hash) UserService {
	return &UserGorm{db, hmac}
}

func (us *UserGorm) Signup(user *UserTable) error {
	return us.db.Create(user).Error
}

func (us *UserGorm) GetUsers() ([]UserTable, error) {
	users := []UserTable{}
	if err := us.db.Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}

func (us *UserGorm) Login(user *Login) (string, error) {

	found := UserTable{}
	err := us.db.Where("email = ?", user.Email).First(&found).Error
	if err != nil {
		return "", err
	}
	err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(user.Password))
	if err != nil {
		return "", err
	}

	token, err := rand.GetToken()
	if err != nil {
		return "", err
	}

	us.hmac.Write([]byte(token))
	expectedMAC := us.hmac.Sum(nil)
	us.hmac.Reset()
	hashToken := base64.URLEncoding.EncodeToString(expectedMAC)

	err = us.db.Model(&UserTable{}).Where("id = ?", found.ID).Update("token", hashToken).Error
	if err != nil {
		return "", err
	}
	return token, nil
}

func (us *UserGorm) GetUserByToken(token string) (*UserTable, error) {

	user := UserTable{}

	us.hmac.Write([]byte(token))
	expectedMAC := us.hmac.Sum(nil)
	us.hmac.Reset()
	hashToken := base64.URLEncoding.EncodeToString(expectedMAC)

	err := us.db.Where("token = ?", hashToken).Find(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, nil

}

func (us *UserGorm) UpdateProfileImg(user_id uint, profile string) error {
	return us.db.Model(&UserTable{}).Where("id = ?", user_id).Update("profile", profile).Error
}

func (us *UserGorm) GetUserBySearch(search string) ([]UserTable, error) {
	usertable := []UserTable{}
	if err := us.db.Where("users.email LIKE ?", "%"+search+"%").Find(&usertable).Error; err != nil {
		return nil, err
	}
	return usertable, nil
}

func (us *UserGorm) GetUserDataById(user_id uint) (*UserTable, error) {
	usertable := UserTable{}
	err := us.db.Where("id = ?", user_id).Find(&usertable).Error
	if err != nil {
		return nil, err
	}
	return &usertable, nil
}

func (us *UserGorm) Logout(user_id uint) error {
	return us.db.Model(&UserTable{}).Where("id = ?", user_id).Update("token", nil).Error
}
