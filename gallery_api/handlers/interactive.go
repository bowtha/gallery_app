package handlers

import (
	"fmt"
	"gallery_api/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Like struct {
	ID      uint
	ImageID uint
	UserID  uint
}

type InteractiveHandler struct {
	is models.InteractiveService
}

func NewInteractiveHandler(is models.InteractiveService) *InteractiveHandler {
	return &InteractiveHandler{is}
}

func (ih *InteractiveHandler) CreateLike(c *gin.Context) {
	id := c.Param("img_id")
	i64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	image_id := uint(i64)

	ids := c.Param("user_id")
	u64, err := strconv.ParseUint(ids, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	user_id := uint(u64)

	like := new(models.LikeTable)
	like.ImageID = image_id
	like.UserID = user_id

	if err := ih.is.CreateLike(like); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}
	c.Status(204)
}

func (ih *InteractiveHandler) GetLikeById(c *gin.Context) {
	id := c.Param("img_id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	image_id := uint(u64)

	liketable, err := ih.is.GetLikeById(image_id)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, liketable)
}
