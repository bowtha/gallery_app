package handlers

import (
	"encoding/base64"
	"fmt"
	"gallery_api/models"
	"gallery_api/rand"
	"hash"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type Signup struct {
	Email string
}

type User struct {
	ID    uint
	Email string
}

type UserHandler struct {
	us   models.UserService
	hmac hash.Hash
}

func NewUserHandler(us models.UserService, hmac hash.Hash) *UserHandler {
	return &UserHandler{us, hmac}
}

func (uh *UserHandler) Signup(c *gin.Context) {
	Email := c.PostForm("Email")
	Password := c.PostForm("Password")

	err := os.MkdirAll("upload", os.ModePerm)
	if err != nil {
		log.Println(err)
		c.Status(500)
		return
	}

	form, err := c.MultipartForm()
	if err != nil {
		c.Status(400)
		return
	}
	file := form.File["Profile"][0]
	err = c.SaveUploadedFile(file, "./upload/"+file.Filename)
	if err != nil {
		c.Status(500)
		return
	}

	signup := new(models.UserTable)
	signup.Email = Email
	signup.Profile = file.Filename
	hash, err := bcrypt.GenerateFromPassword([]byte(Password), 12)
	if err != nil {
		c.Status(500)
		return
	}
	signup.Password = string(hash)

	token, err := rand.GetToken()

	uh.hmac.Write([]byte(token))
	expectedMAC := uh.hmac.Sum(nil)
	uh.hmac.Reset()
	signup.Token = base64.URLEncoding.EncodeToString(expectedMAC)

	err = uh.us.Signup(signup)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"Email": signup.Email,
		"token": token,
	})
}

func (uh *UserHandler) GetUserData(c *gin.Context) {

	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(401, gin.H{
			"message": "invalid token",
		})
		return
	}
	c.JSON(200, user)

}

func (uh *UserHandler) GetUserDataById(c *gin.Context) {

	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	user_id := uint(u64)

	user, err := uh.us.GetUserDataById(user_id)
	if err != nil {
		c.JSON((401), gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, user)

}

type reqLogin struct {
	Email    string
	Password string
}

func (uh *UserHandler) Login(c *gin.Context) {

	login := new(reqLogin)
	if err := c.BindJSON(login); err != nil {
		c.JSON((400), gin.H{
			"message": err.Error(),
		})
		return
	}
	user := new(models.Login)
	user.Email = login.Email
	user.Password = login.Password

	token, err := uh.us.Login(user)
	if err != nil {
		c.JSON((401), gin.H{
			"message": err.Error(),
		})
		return
	}
	userData, err := uh.us.GetUserByToken(token)
	if err != nil {
		c.JSON((401), gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"token": token,
		"ID":    userData.ID,
	})
}

func (uh *UserHandler) Authorization(c *gin.Context) {
	header := c.GetHeader("Authorization")
	fmt.Println("header---->", header)
	token := header[7:]
	fmt.Println("token---->", token)

	user, err := uh.us.GetUserByToken(token)
	if err != nil {
		c.Status(401)
		c.Abort()
		return
	}
	fmt.Printf("%#v", user)
	c.Set("user", user)
}

func (uh *UserHandler) UpdateProfileImg(c *gin.Context) {

	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	user_id := uint(u64)

	err = os.MkdirAll("upload", os.ModePerm)
	if err != nil {
		log.Println(err)
		c.Status(500)
		return
	}

	form, err := c.MultipartForm()
	if err != nil {
		c.Status(400)
		return
	}
	file := form.File["Profile"][0]
	err = c.SaveUploadedFile(file, "./upload/"+file.Filename)
	if err != nil {
		c.Status(500)
		return
	}

	err = uh.us.UpdateProfileImg(user_id, file.Filename)
	if err != nil {
		c.Status(500)
		return
	}
	c.Status(204)

}

type Search struct {
	name string
}

func (uh *UserHandler) GetUserBySearch(c *gin.Context) {

	search := c.Query("search")

	users, err := uh.us.GetUserBySearch(search)
	if err != nil {
		c.Status(500)
		return
	}
	c.JSON(200, users)

}

func (uh *UserHandler) Logout(c *gin.Context) {

	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	user_id := uint(u64)

	err = uh.us.Logout(user_id)
	if err != nil {
		c.Status(500)
		return
	}
	c.Status(204)

}
