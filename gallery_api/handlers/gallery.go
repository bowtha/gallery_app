package handlers

import (
	"fmt"
	"gallery_api/models"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

type GalleryTable struct {
	gorm.Model
	Name   string
	Cover  string
	Status bool
	UserID uint
}

type Gallery struct {
	ID     uint
	Name   string
	Cover  string
	Status bool
	UserID uint
}

type Image struct {
	ID          uint
	Name        string
	Image       string
	GalleryID   uint
	UserProfile string
	UserEmail   string
	UserID      uint
}

type GalleryHandler struct {
	gs models.GalleryService
}

func NewGalleryHandler(gs models.GalleryService) *GalleryHandler {
	return &GalleryHandler{gs}
}

type NewGallery struct {
	Name   string
	Cover  string
	Status bool
}

func (gh *GalleryHandler) GetAllImage(c *gin.Context) {

	tts, err := gh.gs.GetAllImage()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	images := []Image{}
	for _, tt := range tts {
		images = append(images, Image{
			ID:          tt.ID,
			Name:        tt.Name,
			Image:       tt.Image,
			GalleryID:   tt.GalleryID,
			UserProfile: tt.UserProfile,
			UserEmail:   tt.UserEmail,
			UserID:      tt.UserID,
		})
	}
	c.JSON(http.StatusOK, images)
}

func (gh *GalleryHandler) GetAllImageSort(c *gin.Context) {

	tts, err := gh.gs.GetAllImageSort()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	images := []Image{}
	for _, tt := range tts {
		images = append(images, Image{
			ID:          tt.ID,
			Name:        tt.Name,
			Image:       tt.Image,
			GalleryID:   tt.GalleryID,
			UserProfile: tt.UserProfile,
			UserEmail:   tt.UserEmail,
			UserID:      tt.UserID,
		})
	}
	c.JSON(http.StatusOK, images)
}

func (gh *GalleryHandler) GetAllGallery(c *gin.Context) {
	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	user_id := uint(u64)

	tts, err := gh.gs.GetAllGallery(user_id)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	gallerys := []Gallery{}
	for _, tt := range tts {
		gallerys = append(gallerys, Gallery{
			ID:     tt.ID,
			Name:   tt.Name,
			Cover:  tt.Cover,
			Status: tt.Status,
		})
	}
	c.JSON(http.StatusOK, gallerys)
}

func (gh *GalleryHandler) GetGallery(c *gin.Context) {
	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	gallery_id := uint(u64)

	tts, err := gh.gs.GetGallery(gallery_id)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	gallerys := []Gallery{}
	for _, tt := range tts {
		gallerys = append(gallerys, Gallery{
			ID:     tt.ID,
			Name:   tt.Name,
			Cover:  tt.Cover,
			Status: tt.Status,
		})
	}
	c.JSON(http.StatusOK, gallerys)
}

func (gh *GalleryHandler) GetImageList(c *gin.Context) {
	id := c.Param("gallery_id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	gallery_id := uint(u64)

	tts, err := gh.gs.GetImageList(gallery_id)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	images := []Image{}
	for _, tt := range tts {
		images = append(images, Image{
			ID:        tt.ID,
			Name:      tt.Name,
			Image:     tt.Image,
			GalleryID: tt.GalleryID,
		})
	}
	c.JSON(http.StatusOK, images)
}

func (gh *GalleryHandler) CreateGallery(c *gin.Context) {

	err := os.MkdirAll("upload", os.ModePerm)
	if err != nil {
		log.Println(err)
		c.Status(500)
		return
	}

	id := c.PostForm("UserID")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	UserID := uint(u64)
	Name := c.PostForm("Name")
	form, err := c.MultipartForm()
	if err != nil {
		c.Status(400)
		return
	}
	log.Printf("%v\n", Name)
	file := form.File["Cover"][0]
	log.Println(file)

	err = c.SaveUploadedFile(file, "./upload/"+file.Filename)
	if err != nil {
		c.Status(500)
		return
	}
	galleryTable := new(models.GalleryTable)
	galleryTable.Name = Name
	galleryTable.Cover = file.Filename
	galleryTable.Status = false
	galleryTable.UserID = UserID
	if err := gh.gs.CreateGallery(galleryTable); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusCreated, Gallery{
		ID:     galleryTable.ID,
		Name:   galleryTable.Name,
		Cover:  galleryTable.Cover,
		Status: galleryTable.Status,
		UserID: galleryTable.UserID,
	})
}

func (gh *GalleryHandler) CreateImage(c *gin.Context) {
	Name := c.PostForm("Name")
	id := c.PostForm("GalleryID")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	GalleryID := uint(u64)

	err = os.MkdirAll("upload", os.ModePerm)
	if err != nil {
		log.Println(err)
		c.Status(500)
		return
	}

	form, err := c.MultipartForm()
	if err != nil {
		c.Status(400)
		return
	}
	file := form.File["Image"][0]
	err = c.SaveUploadedFile(file, "./upload/"+file.Filename)
	if err != nil {
		c.Status(500)
		return
	}
	imageTable := new(models.ImageTable)
	imageTable.Name = Name
	imageTable.Image = file.Filename
	imageTable.GalleryID = GalleryID
	if err := gh.gs.CreateImage(imageTable); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(http.StatusCreated, Image{
		ID:        imageTable.ID,
		Name:      imageTable.Name,
		Image:     imageTable.Image,
		GalleryID: imageTable.GalleryID,
	})
}

func (gh *GalleryHandler) UpdateGalleryName(c *gin.Context) {
	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	gallery_id := uint(u64)

	name := c.PostForm("Name")
	err = gh.gs.UpdateGalleryName(gallery_id, name)
	if err != nil {
		c.Status(500)
		return
	}
	c.Status(204)
}

func (gh *GalleryHandler) UpdateGalleryStatus(c *gin.Context) {

	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	gallery_id := uint(u64)

	req := struct {
		Status bool `json:"status"`
	}{}
	if err := c.BindJSON(&req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	err = gh.gs.UpdateGalleryStatus(gallery_id, req.Status)
	if err != nil {
		c.Status(500)
		return
	}
	c.Status(204)
}

func (gh *GalleryHandler) UpdateGalleryCover(c *gin.Context) {
	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	gallery_id := uint(u64)

	err = os.MkdirAll("upload", os.ModePerm)
	if err != nil {
		log.Println(err)
		c.Status(500)
		return
	}

	form, err := c.MultipartForm()
	if err != nil {
		c.Status(400)
		return
	}
	file := form.File["Cover"][0]
	err = c.SaveUploadedFile(file, "./upload/"+file.Filename)
	if err != nil {
		c.Status(500)
		return
	}
	err = gh.gs.UpdateGalleryCover(gallery_id, file.Filename)
	if err != nil {
		c.Status(500)
		return
	}
	c.Status(204)
}

func (gh *GalleryHandler) DeleteGallery(c *gin.Context) {
	ids := c.Param("id")
	u64, err := strconv.ParseUint(ids, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	id := uint(u64)

	tts, err := gh.gs.GetGallery(id)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	gallerys := []Gallery{}
	for _, tt := range tts {
		err = os.Remove(filepath.Join("upload", tt.Cover))
		gallerys = append(gallerys, Gallery{
			ID:     tt.ID,
			Name:   tt.Name,
			Cover:  tt.Cover,
			Status: tt.Status,
		})
	}

	if err := gh.gs.DeleteGallery(id); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}
	c.Status(204)
}

func (gh *GalleryHandler) DeleteAllImage(c *gin.Context) {
	ids := c.Param("id")
	u64, err := strconv.ParseUint(ids, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	id := uint(u64)

	tts, err := gh.gs.GetImageList(id)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	images := []Image{}
	for _, tt := range tts {
		err = os.Remove(filepath.Join("upload", tt.Image))
		images = append(images, Image{
			ID:        tt.ID,
			Name:      tt.Name,
			Image:     tt.Image,
			GalleryID: tt.GalleryID,
		})
	}

	err = gh.gs.DeleteAllImage(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}

	c.Status(204)
}

func (gh *GalleryHandler) DeleteImage(c *gin.Context) {
	ids := c.Param("id")
	u64, err := strconv.ParseUint(ids, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	id := uint(u64)

	if err = gh.gs.DeleteImage(id); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}
	c.Status(204)
}

func (gh *GalleryHandler) GetGalleryBYUserID(c *gin.Context) {
	id := c.Param("user_id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	user_id := uint(u64)

	tts, err := gh.gs.GetGalleryBYUserID(user_id)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	gallerys := []Gallery{}
	for _, tt := range tts {
		gallerys = append(gallerys, Gallery{
			ID:     tt.ID,
			Name:   tt.Name,
			Cover:  tt.Cover,
			Status: tt.Status,
		})
	}
	c.JSON(http.StatusOK, gallerys)
}
